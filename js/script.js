$(document).ready(() => {
    $(".img-box").each((i, n) => { $(n).click(e => boxClick(e, i)); });
    $("#txtInput").on("keypress", (e) => txtInputValidate(e));
    $("#txtInput").on("paste", (e) => { e.preventDefault(); return false; });
    $("#btnCreate").click(e => btnCreateClick(e));
    $("#btnRepeat").click(e => btnRepeatClick(e));
    $("#win-score").hover(() => { $("#win-tooltop").toggle(75); });
    $("#lose-score").hover(() => { $("#lose-tooltop").toggle(75); });
    
    $("#win-score").text(store.wins);
    $("#lose-score").text(store.loses);
    
    store.newAttempts();
    store.newRandom();       
});

let store = new Store();

function Store(){
    this.attempts = 0;
    this.wins = 0;
    this.loses = 0;
    this.random = 0;
    
    this.newAttempts = (n) => {
        this.attempts = n != null || typeof(n) == "number" ? n : ((Math.floor($(".img-box").length / 10) + 1) * 2);
        $(".counter").text(this.attempts);
    };
    this.newRandom = (n) => {
        this.random = n != null || typeof(n) == "number" ? n : Math.floor((Math.random() * $(".img-box").length)); 
        console.log("Приз в кадре " + (this.random + 1)); 
    };
}

function boxClick(e, ind){ 
    if ($(e.target).css("background-color") == "rgb(219, 112, 147)" ||
        $(e.target).css("background-color") == "rgb(152, 251, 152)"){
        return;
    }
    
    if (ind == store.random){
        $(e.target).css({"background-color": "palegreen"});
        $(e.target).parent().children(".img-text").text("УГАДАЛ!");
        $(e.target).parent().children(".img-text").css({"color": "green"});
        $(e.target).parent().children(".img-text").slideDown(300);
        
        store.wins += 1;
    }
    else{
        $(e.target).css({"background-color": "palevioletred"});
        $(e.target).parent().children(".img-text").text("МИМО");
        $(e.target).parent().children(".img-text").css({"color": "red"});
        $(e.target).parent().children(".img-text").slideDown(300);
    }
    
    $(".counter").text(Number($(".counter").text()) - 1);
    
    if (Number($(".counter").text()) == 0 || ind == store.random){
        $(".img-box").unbind("click");
        
        $(".img-text").each((i, n) => {
            if ($(n).css("display") == "none"){
                $(n).css({"color": "black"}).slideDown(300);
                $(n).parent().children(".img-box").css({"background-color": "lightgrey"});
                $(n).text("-");
                
                if (i == store.random){
                    $(n).text("-ПРИЗ-");
                    $(n).css({"display": "block", "color": "green"});
                    store.loses += 1;
                } 
            }
        });
        
        $(".counter").text("0");
    }   
    
    $("#win-score").text(store.wins);
    $("#lose-score").text(store.loses);
}

function txtInputValidate(e){   
    if (e.keyCode == 13){
        $("#btnInput").click();
        e.preventDefault();
        return;
    } 
    
    if (e.keyCode < 48 || e.keyCode > 57){
        e.preventDefault();
        e.returnValue = false;
    }
    
    let val = Number(String.fromCharCode(e.keyCode));
    
    if (Number.isNaN(val)){
        e.preventDefault();
        e.returnValue = false;
    } 
    else e.returnValue = true;
}

function btnCreateClick(e){
    let txtVal = Number($("#txtInput").val());
    
    if (txtVal < 3 || txtVal > 200 || Number.isNaN(txtVal)){
        alert("Допустимый целочисленный диапазон значений от 3 до 200!");
        return;
    }
    
    if (txtVal < $(".img-box").length){
        while ($(".img-box").length != txtVal)
            $(".main").children().last().remove();
    }
    else if (txtVal > $(".img-box").length){
        while ($(".img-box").length != txtVal)
            $("#guess-box")
                .clone().children(".img-box")
                .text("#" + ($(".img-box").length + 1))
                .parent().appendTo(".main");            
    }
    
    $(".img-box").each((i, n) => { 
        $(n).css({"background-color": "white"});
        $(n).parent().children(".img-text").css({"color": "black"});
        $(n).parent().children(".img-text").slideUp(300);
    });
    
    $(".img-box").each((i, n) => { $(n).unbind("click"); });
    $(".img-box").each((i, n) => { $(n).click(e => boxClick(e, i)); });
      
    store.newRandom();    
    store.newAttempts();
    $("#txtInput").val("");
}

function btnRepeatClick(e){
    $(".img-box").each((i, n) => { 
        $(n).css({"background-color": "white"});
        $(n).parent().children(".img-text").slideUp(300);
    });
    
    $(".img-box").each((i, n) => { $(n).unbind("click"); });
    $(".img-box").each((i, n) => { $(n).click(e => boxClick(e, i)); });
    
    store.newRandom();   
    store.newAttempts();
}